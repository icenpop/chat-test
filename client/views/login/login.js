Template.login.events({

	"click .loginButton": function () {
		
		var username = $("#loginUsername").val();
		var password = $("#loginPassword").val();

		Meteor.loginWithPassword(username, password, function(loginError) {
			if(loginError) {
				alert(loginError.reason);
			}
		});

	},

	"click .loginGoToRegister": function  () {
		Router.go("/register")
	},

	"click .loginWithGoogle": function() {

		Meteor.loginWithGoogle({}, function(googleLoginError) {
			
			if(googleLoginError) {
				
				alert(googleLoginError.reason);

			}
			
		});
		
	}

})