
Template.lobby.events(topBarCommonEvents);

Template.lobby.events({

	"click .newRoomCreate": function() {

		var chatRoomName = "testname";

		ChatRooms.insert({
			owner: Meteor.userId(),
			name: $("#newRoom").val()
		});

		$(".newRoomOverlay").toggleClass("hidden")
		

	},

	"click .chatRoom": function() {

		Router.go("/room/" + this._id);

	},

	"click .createChatRoom": function  () {
		$(".newRoomOverlay").toggleClass("hidden")
	}

});

Template.lobby.helpers({

	chatRooms: function () {
		
		return ChatRooms.find();

	},

	userName: function() {

		var user = Meteor.user({}, getCurrentUserNameFields);
		if(!user) return;

		return getCurrentUserName(user);

	},

	messageCountFormatted: function() {

		var messageCount = 0;

		if(this.messageCount) {
			messageCount = this.messageCount;
		}

		var messageSuffix = "Message";
		if(messageCount != 1) {
			messageSuffix += "s";
		}

		return messageCount + " " + messageSuffix;

	}

});