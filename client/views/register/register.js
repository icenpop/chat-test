
Template.register.events({
	"click .registerUser": function () {
		
		var username = $("#registerUsername").val();
		var password = $("#registerPassword").val();

		Accounts.createUser({
			username: username,
			password: password
		}, function(registerError) {
			if(registerError){
				alert(registerError.reason);
			}
		});

	},

	"click .registerGoToLogin": function  () {
		
		Router.go("/login");

	},

	"click .registerWithGoogle": function() {

		Meteor.loginWithGoogle({}, function(googleLoginError) {
			
			if(googleLoginError) {
				
				alert(googleLoginError.reason);

			}
			
		});

	}
})