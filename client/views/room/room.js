
Template.room.events(topBarCommonEvents);

Template.room.helpers({

	chatRoomName: function() {

		var chatRoomId = Router.current().params._id;
	
		var chatRoom = ChatRooms.findOne(chatRoomId);
		if(!chatRoom) return;

		var chatRoomName = chatRoom.name;

		return chatRoomName;

	},

	chatMessages: function() {

		return ChatMessages.find();

	},

	chatMessageFormattedDate: function() {

		if(!this.postedAt) return;

		var postedAtDate = new Date(this.postedAt);

		var postedAtString = postedAtDate.format("mm/dd/yy, h:MM:ss TT");

		return postedAtString;

	}

})

Template.room.events({

	"click .backButton": function() {

		Router.go("/");

	},

	"click .postMessage": function () {
		
		var message = $(".newMessageContent").val();

		var chatRoomId = Router.current().params._id;

		ChatMessages.insert({
			
			owner: Meteor.userId(),
			message: message,
			chatRoom: chatRoomId

		});

	}

});