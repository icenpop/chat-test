
var authViews = ["login", "register"];

Tracker.autorun(function() {

	if(!Router.current()) return;

	//Check if an auth view
	var routeName = Router.current().route.getName();

	var isAuthView = isView(routeName, authViews);

	var isLoggedIn = !((!(Meteor.user() || Meteor.loggingIn())));
	
	if (!isLoggedIn && !isAuthView) {
		
		//Redirect logged out users to /login, except for if they're on any of the auth views
		Router.go("login");

	} else if (isAuthView && isLoggedIn) {
	
		//Redirect logged in users to /, if they are on the auth views
		Router.go("/");
		
	}

});


Router.route('/login');

Router.route('/register');



// Main routes

Router.route('/', {
	
	waitOn: function() {
		return [Meteor.subscribe("chatRooms")];
	},

	action: function () {
		this.render('lobby');
	}

});

Router.route('/room/:_id', {

	waitOn: function() {
		return [Meteor.subscribe("chatRooms"), Meteor.subscribe("chatMessages", this.params._id)];
	},

	action: function () {
		this.render('room');
	}

});

