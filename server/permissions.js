
ChatRooms.allow({

	insert: function (userId, doc) {
		return (userId && doc.owner === userId);
	},

	update: function (userId, doc) {
		return (userId && doc.owner === userId);
	},

	remove: function () {
		return false;
	}

});

ChatMessages.allow({

	insert: function(userId, doc) {
		return (userId && doc.owner === userId);
	},

	update: function() {
		return false;
	},

	remove: function() {
		return false;
	}

});