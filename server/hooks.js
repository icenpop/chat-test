
ChatMessages.before.insert(function (userId, doc) {

	var poster = Meteor.users.findOne(userId, getCurrentUserNameFields);

	var ownerName = getCurrentUserName(poster);

	var unixEpoch = Date.now();

	doc.postedAt = unixEpoch;
	doc.ownerName = ownerName;

});

ChatMessages.after.insert(function (userId, doc) {

	check(doc.chatRoom, String);

	var chatMessagesCount = ChatMessages.find({
		chatRoom: doc.chatRoom
	}).count();

	ChatRooms.update(doc.chatRoom, {
		$set: {
			messageCount: chatMessagesCount
		}
	});

});