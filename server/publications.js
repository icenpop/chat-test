
Meteor.publish("chatRooms", function () {
	
	return ChatRooms.find({}, {
		fields: {
			name: 1,
			messageCount: 1
		}
	});

});

Meteor.publish("chatMessages", function(chatRoom) {

	check(chatRoom, String);

	return ChatMessages.find({
		chatRoom: chatRoom
	}, {
		fields: {
			message: 1,
			ownerName: 1,
			postedAt: 1
		}
	});

});