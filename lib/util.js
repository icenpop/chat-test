isView = function(routeName, views) {
	var isChosenView = false;

	if(typeof routeName == "undefined") return false;
	routeName = routeName.split(".")[0];

	for (var i = 0; i < views.length; i++) {
		var view = views[i].split(".")[0];
		if(view == routeName) {
			isChosenView = true;
		}
	};

	return isChosenView;
}

getCurrentUserNameFields = {
	fields: {
		"username": 1,
		"profile.name": 1
	}
};

getCurrentUserName = function(user) {

	if(user.username) {
		return user.username;
	} else if (user.profile) {
		if(user.profile.name) {
			return user.profile.name;
		}
	}

	return "Unnamed";

}